package edu.ntust.cs.ss.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;

import edu.ntust.cs.ss.login.adapter.MessageAdapter;
import edu.ntust.cs.ss.login.domain.MessageContent;
import edu.ntust.cs.ss.login.lib.AESGenerator;
import edu.ntust.cs.ss.login.lib.DB;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;


import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;



/**
 * @author  Ssu-Wei,Tang
 */
public class MessageActivity extends ActionBarActivity {
    private DB mDbHelper = new DB(MessageActivity.this);
    private ListView listviewMessage;
    private MessageAdapter messageAdapter;
    private String token;
    private String loginStatus;
    private ArrayList<MessageContent> listMessage;
    private MessageContent[] messageContent;
    private Button buttonHead;
    private TextView menuTextViewName;
    private String myAesKey;
    private String aesToken;
    private String myName;
    private String myhead;
    public static ProgressDialog progressDialog;
    private static Context mContext;
    private Menu mymenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_message);
        mDbHelper.open();
        new pageStart().execute();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        mymenu=menu;
        getMenuInflater().inflate(R.menu.menu_message, menu);





//        menuTextViewName=(TextView)menu.findItem(R.id.action_head).getActionView().findViewById(menu_textViewName);

//        buttonHead = (Button) menu.findItem(R.id.action_head).getActionView().findViewById(R.id.menu_buttonHead);



        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        
        if (id == R.id.action_head) {


            System.out.println(myhead);
            System.out.println(myName);


            return true;
        }

        if (id == R.id.action_post) {

            Intent intent = new Intent();
            intent.setClass(MessageActivity.this, PostMessageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("myAesKey", myAesKey);
            intent.putExtras(bundle);
            startActivity(intent);




            return true;
        }

        if (id == R.id.action_logout) {



            mDbHelper.deleteTokenData();
            mDbHelper.deleteLoginAccount();
            mDbHelper.deleteShaTokenData();

            Intent intent = new Intent();
            intent.setClass(MessageActivity.this, LoginActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("logout", "T");
            intent.putExtras(bundle);
            startActivity(intent);
            finish();





            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    public  ProgressDialog showProgressDialog() {
        progressDialog = new ProgressDialog(MessageActivity.this);
        progressDialog.setMessage("load Message...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }

    @Override
    public void onDestroy() {
        mDbHelper.close();
        super.onDestroy();
    }



    private String getSqliteToken() {

        String myToken = null;
        Cursor cursorToken = mDbHelper.getTokenData();
        while (cursorToken.moveToNext()) {
            myToken = cursorToken.getString(0);
        }
        cursorToken.close();
        return myToken;
    }

    private String getToken() {

        String deAesToken=null;
        Bundle secretBundle = MessageActivity.this.getIntent().getExtras();
        myAesKey = secretBundle.getString("myAesKey");



            aesToken= getSqliteToken();


        try {
            deAesToken= AESGenerator.DecryptToken(aesToken, myAesKey);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return deAesToken;
    }
    private class pageStart extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Bundle secretBundle = MessageActivity.this.getIntent().getExtras();
            myAesKey = secretBundle.getString("myAesKey");

            showProgressDialog();
            listviewMessage = (ListView) findViewById(R.id.listviewMessage);

        }

        @Override
        protected String doInBackground(Void... arg0) {



            listMessage = new ArrayList<MessageContent>();
            try {
                String myuserUrl="https://idsl.bfx.tw/user/query.json?user[token]=".concat(getToken());

                JSONObject myusersJsonObject =new JSONObject(requestHTTPSPage(myuserUrl));

                myhead=myusersJsonObject.getString("avatar_url");
                myName=myusersJsonObject.getString("first_name");
                myhead.replace("\\u0026", "&");
                myhead.replace("https","http");

                JSONArray msgJsonArray = new JSONArray(requestHTTPSPage("https://idsl.bfx.tw/articles.json"));
              messageContent = new MessageContent[msgJsonArray.length()];

            for (int i = 0; i < msgJsonArray.length(); i++) {

                JSONObject msgJsonObject = msgJsonArray.getJSONObject(i);
                messageContent[i] = new MessageContent();
                messageContent[i].setMessage(msgJsonObject.getString("content"));
                messageContent[i].setMood(msgJsonObject.getString("emotion"));
                messageContent[i].setTime(msgJsonObject.getString("created_at"));



                String UserId=msgJsonObject.getString("user_id");



                String url="https://idsl.bfx.tw/users/".concat(UserId).concat(".json");


                JSONObject usersJsonObject =new JSONObject(requestHTTPSPage(url));


                messageContent[i].setHead(usersJsonObject.getString("avatar_url").replace("\\u0026", "&"));
                messageContent[i].setName(usersJsonObject.getString("first_name"));



                listMessage.add(messageContent[i]);

            }

            } catch (JSONException e) {

                    e.printStackTrace();
                }




            return null;
        }

      private  String requestHTTPSPage(String mUrl) {
            InputStream ins = null;
            String result = "";
            try {
                ins = MessageActivity.this.getAssets().open("ca.crt");
                CertificateFactory cerFactory = CertificateFactory
                        .getInstance("X.509");
                Certificate cer = cerFactory.generateCertificate(ins);
                KeyStore keyStore = KeyStore.getInstance("PKCS12", "BC");
                keyStore.load(null, null);
                keyStore.setCertificateEntry("trust", cer);

                SSLSocketFactory socketFactory = new SSLSocketFactory(keyStore);
                Scheme sch = new Scheme("https", socketFactory, 443);
                HttpClient mHttpClient = new DefaultHttpClient();
                mHttpClient.getConnectionManager().getSchemeRegistry()
                        .register(sch);




                BufferedReader reader = null;
                try {
                    Log.d("CA", "executeGet is in,murl:" + mUrl);
                    HttpGet request = new HttpGet();
                    request.setURI(new URI(mUrl));
                    HttpResponse response = mHttpClient.execute(request);
                    if (response.getStatusLine().getStatusCode() != 200) {
                        request.abort();
                        return result;
                    }

                    reader = new BufferedReader(new InputStreamReader(response
                            .getEntity().getContent()));
                    StringBuffer buffer = new StringBuffer();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }
                    result = buffer.toString();
                    Log.d("CA", "mUrl=" + mUrl + "\nresult = " + result);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        reader.close();
                    }
                }
            } catch (Exception e) {

            } finally {
                try {
                    if (ins != null)
                        ins.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;
        }


        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            mymenu.findItem(R.id.action_head).setTitle(myName);
//            mymenu.findItem(R.id.action_head).setIcon(R.drawable.person);
            messageAdapter=new MessageAdapter(MessageActivity.this, listMessage);


            listviewMessage.setAdapter(messageAdapter);
            listviewMessage.setOnItemClickListener(MessageSetItemClick);
            progressDialog.dismiss();


        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    private OnItemClickListener MessageSetItemClick = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> a, View v, int position, long id) {


        }
    };

    private String getJSONUrl(String url) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }




}
