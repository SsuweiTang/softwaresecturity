package edu.ntust.cs.ss.login.lib;


import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AESGenerator {


    public  static String generateKey(String key){
        int length = key.length();
        if( length < 16 ) {
            for( int i=length ;i<16; ++i )
                key += i%10;
            return key;
        } else if ( length < 24 ) {
            for( int i=length ;i<24; ++i )
                key += i%10;
            return key;
        } else if ( length < 32 ) {
            for( int i=length ;i<32; ++i )
                key += i%10;
            return key;
        }
        return key.substring(0, 32);
    }

    public static String EncryptToken(String plainToken, String key) throws Exception{
        SecretKeySpec spec = new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, spec);
            return Base64.encodeToString(cipher.doFinal(plainToken.getBytes()), android.util.Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String DecryptToken(String data, String key) throws Exception {
        SecretKeySpec spec = new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, spec);
            return new String( cipher.doFinal(Base64.decode(data, android.util.Base64.NO_WRAP)) );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
