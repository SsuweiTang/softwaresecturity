package edu.ntust.cs.ss.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import edu.ntust.cs.ss.login.lib.DB;

/**
 * @author Ssu-Wei,Tang
 */
public class SplashActivity extends Activity {

    ImageView imageViewSplash;
    int screenWidth;
    int screenHeight;
    WindowManager windowManager;
    private DB mDbHelper = new DB(SplashActivity.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        screenWidth = windowManager.getDefaultDisplay().getWidth();
        screenHeight = windowManager.getDefaultDisplay().getHeight();
        mDbHelper.open();
        findView();
        Loading();
    }

    private void Loading() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                jumpToSecret();
            }
        }, 2000);

    }

    @Override
    public void onDestroy() {
        mDbHelper.close();
        super.onDestroy();
    }

    protected void jumpToSecret() {
        Intent intent = new Intent();
        intent.setClass(SplashActivity.this, SecretActivity.class);
        startActivity(intent);
        finish();

    }

    private void findView() {
        imageViewSplash= (ImageView) findViewById(R.id.imageViewLoadIcon);
        imageViewSplash.getLayoutParams().height = screenHeight;
        imageViewSplash.getLayoutParams().width = screenWidth;

    }



}
