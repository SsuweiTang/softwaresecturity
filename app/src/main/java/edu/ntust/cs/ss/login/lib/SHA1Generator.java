package edu.ntust.cs.ss.login.lib;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import java.util.Formatter;

import android.util.Base64;

/**
 *
 * @author Ssu-Wei,Tang
 *
 */
public class SHA1Generator {


    private static final String ALGORITHM = "SHA-256";
    private static MessageDigest md;
    private static byte[] digets;

    public static String generateSha1ForString(String text) {
        String HEXdigets = null;
        try {
            md = MessageDigest.getInstance(ALGORITHM);
            md.reset();
            md.update(text.getBytes("UTF-8"));
            digets = md.digest();
            HEXdigets = convertDigestsToHex(digets);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return HEXdigets;
    }

    private static String convertDigestsToHex(byte[] digets) {
        String HEXdigets = "";
        try (Formatter formatter = new Formatter()) {
            for (final byte b : digets) {
                formatter.format("%02x", b);
            }
            HEXdigets = formatter.toString();
        }
        return HEXdigets;
    }


}
