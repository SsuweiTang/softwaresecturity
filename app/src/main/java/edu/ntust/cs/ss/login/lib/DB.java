package edu.ntust.cs.ss.login.lib;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author  Ssu-Wei,Tang
 */
public class DB {
    private Context mCtx = null;
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;

    public DB(Context ctx) {
        this.mCtx = ctx;
    }

    public DB open() throws SQLException {
        dbHelper = new DatabaseHelper(mCtx);
        db = dbHelper.getWritableDatabase();

        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void beginTransaction() {
        db.beginTransaction();
    }

    public void transactionSuccessful() {
        db.setTransactionSuccessful();
    }

    public void endTransaction() {
        db.endTransaction();
    }


    public static final String TABLE_TOKEN = "token";
    public static final String ACCESS_TOKEN_KEY = "access_token";

    public static final String TABLE_SHATOKEN = "sha_token";
    public static final String ACCESS_SHATOKEN_KEY = "access_shatoken";


    public static final String TABLE_LOGIN_ACCOUNT = "login_account";
    public static final String KEY_ACCOUNT_NO = "account_no";
    public static final String KEY_ACCOUNT_ID = "account_id";



    // insert sqlite
    public long insertTokenData(String token) {
        ContentValues args = new ContentValues();
        args.put(ACCESS_TOKEN_KEY, token);

        return db.insert(TABLE_TOKEN, null, args);
    }

    public long insertShaTokenData(String shatoken) {
        ContentValues args = new ContentValues();
        args.put(ACCESS_SHATOKEN_KEY, shatoken);

        return db.insert(TABLE_SHATOKEN, null, args);
    }

    public long insertLoginAccount(String no,String id) {
        ContentValues args = new ContentValues();
        args.put(KEY_ACCOUNT_NO, no);

        args.put(KEY_ACCOUNT_ID, id);

        return db.insert(TABLE_LOGIN_ACCOUNT, null, args);
    }

    // getData

    public Cursor getTokenData() {
        return db.rawQuery("select * from token", null);
    }

    public Cursor getShaTokenData() {
        return db.rawQuery("select * from sha_token", null);
    }

    public Cursor getLoginAccount() {
        return db.rawQuery("select * from login_account", null);
    }



    // deleteData

    public int deleteTokenData() {
        return db.delete(TABLE_TOKEN, null, null);
    }
    public int deleteShaTokenData() {
        return db.delete(TABLE_SHATOKEN, null, null);
    }

    public int deleteLoginAccount() {
        return db.delete(TABLE_LOGIN_ACCOUNT, null, null);
    }


    // CREATE table
    private static final String DATABASE_NAME = "ss.db";
    private static final int DATABASE_VERSION =2;
    private static final String CREATE_TOKEN = "CREATE TABLE IF NOT EXISTS token("
            + "access_token varchar primary key not null" + ");";
    private static final String CREATE_SHATOKEN = "CREATE TABLE IF NOT EXISTS sha_token("
            + "access_shatoken varchar primary key not null" + ");";

    private static final String CREATE_LOGIN_ACCOUNT = "CREATE TABLE IF NOT EXISTS login_account("
            + "account_no varchar,"
            + "account_id varchar"
            + ");";


    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TOKEN);
            db.execSQL(CREATE_SHATOKEN);
            db.execSQL(CREATE_LOGIN_ACCOUNT);


            System.out.println("onCreate DB");

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            onCreate(db);
        }
    }

}
