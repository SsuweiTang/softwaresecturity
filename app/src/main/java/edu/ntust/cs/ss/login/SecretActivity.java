package edu.ntust.cs.ss.login;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import edu.ntust.cs.ss.login.lib.AESGenerator;
import edu.ntust.cs.ss.login.lib.DB;
import edu.ntust.cs.ss.login.lib.SHA1Generator;
import android.widget.TextView.OnEditorActionListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class SecretActivity extends Activity {
    private EditText editTextPasscode;
    private DB mDbHelper = new DB(SecretActivity.this);
    private String  loginStatus;
    private String secret;
    private String ShaToken;
    private String FBId;
    private String myAesKey;
    private String myhead;
    private String myName;
    private String aesToken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secret);
        mDbHelper.open();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        findViewById();

    }

    private void findViewById() {
        editTextPasscode = (EditText) findViewById(R.id.Passcode);

        deterLogin();
        if (loginStatus!=null) {
            if(loginStatus.toString().equals("T")) {
                ShaToken = getShaToken();
            }
        }
        editTextPasscode.setImeOptions(EditorInfo.IME_ACTION_DONE);

        editTextPasscode.setOnEditorActionListener(new OnEditorActionListener() {

            public boolean onEditorAction(TextView v, int keycode, KeyEvent event) {


                if (keycode == KeyEvent.ACTION_DOWN || keycode == EditorInfo.IME_ACTION_DONE) {
                    if (editTextPasscode.getText().toString().length() == 4) {
                        secret = editTextPasscode.getText().toString();

                        myAesKey = AESGenerator.generateKey(secret);

                        if (loginStatus != null) {
                            if (loginStatus.toString().equals("T")) {
                                if (SHA1Generator.generateSha1ForString(secret.concat(FBId)).equals(ShaToken)) {


                                    jumpToMessage();


                                } else {


                                    Toast.makeText(SecretActivity.this, "secret error!", Toast.LENGTH_LONG).show();


                                }
                            } else {


                                jumpToLogin();
                            }
                        } else {


                            jumpToLogin();
                        }


                    } else {
                        Toast.makeText(SecretActivity.this, "please enter four digits!", Toast.LENGTH_LONG).show();
                    }
                    return true;
                }
                return false;

            }
        });
    }




    @Override
    public void onDestroy() {
        mDbHelper.close();
        super.onDestroy();
    }

    protected void jumpToMessage() {
        Intent intent = new Intent();
        intent.setClass(SecretActivity.this, MessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("myAesKey", myAesKey);
        bundle.putString("Secret", secret);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();

    }
    protected void jumpToLogin() {
        Intent intent = new Intent();
        intent.setClass(SecretActivity.this, LoginActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("myAesKey", myAesKey);
        bundle.putString("Secret", secret);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();

    }

    private void deterLogin() {

        Cursor cursorLoginAccount = mDbHelper.getLoginAccount();
        while (cursorLoginAccount.moveToNext()) {
            loginStatus = cursorLoginAccount.getString(0);
            FBId= cursorLoginAccount.getString(1);
        }




        cursorLoginAccount.close();

    }
    private String  getShaToken() {
        String shaToken = null;
        Cursor cursorShaToken = mDbHelper.getShaTokenData();
        while (cursorShaToken.moveToNext()) {
            shaToken = cursorShaToken.getString(0);
        }



        cursorShaToken.close();

        return shaToken;

    }









}
