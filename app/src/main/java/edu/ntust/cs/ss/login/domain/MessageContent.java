package edu.ntust.cs.ss.login.domain;

import java.io.Serializable;

/**
 * @author Ssu-Wei,Tang
 */
public class MessageContent implements Serializable {
    private static final long serialVersionUID = 1L;

    String name;
    String head;
    String time;
    String message;
    String mood;
    String userId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



}
