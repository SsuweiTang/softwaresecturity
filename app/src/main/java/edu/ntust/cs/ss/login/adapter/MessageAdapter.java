package edu.ntust.cs.ss.login.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import edu.ntust.cs.ss.login.R;
import edu.ntust.cs.ss.login.domain.MessageContent;
import edu.ntust.cs.ss.login.lib.ImageHandle;


/**
 * @author Ssu-Wei,Tang
 */
public class MessageAdapter extends BaseAdapter {


    private MessageHolder holder = null;
    private Context ct2;
    private ArrayList<MessageContent> items;
    private String Head;

    private boolean view_tag;


    public MessageAdapter(Context context, ArrayList<MessageContent> items) {
        this.items = items;
        this.ct2 = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }


    public void show_channel_view(boolean view){
        view_tag = view;
    }



    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            holder = new MessageHolder();
            v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.listview_message, null);
            holder.TextViewMessage = (TextView) v.findViewById(R.id.textViewMessage);
            holder.TextViewName = (TextView) v.findViewById(R.id.textViewName);
            holder.TextViewTime = (TextView) v.findViewById(R.id.textViewPostTime);
            holder.ImageViewHead = (ImageView) v.findViewById(R.id.imageViewHead);
            holder.ImageViewMood = (ImageView) v.findViewById(R.id.imageViewMood);

            v.setTag(holder);
        } else {
            holder = (MessageHolder) v.getTag();
        }

        MessageContent o = items.get(position);
        if (o != null) {
            if (holder.TextViewName != null) {
                holder.TextViewName.setText(o.getName());
            }
            if (holder.TextViewMessage != null) {
                holder.TextViewMessage.setText(o.getMessage());

            }
            if (holder.TextViewTime != null) {
                holder.TextViewTime.setText(o.getTime());

            }
            if (holder.ImageViewHead != null) {

                if(o.getHead()!=null)
                {

                    Head=o.getHead().toString();
                    new pageStart().execute();


                }
                else
                {
                    holder.ImageViewHead.setImageResource(R.drawable.person);
                }
            }
            if (holder.ImageViewMood != null) {
                if(o.getMood().equals("1"))
                holder.ImageViewMood.setBackgroundResource(R.drawable.cry);
                if(o.getMood().equals("2"))
                    holder.ImageViewMood.setBackgroundResource(R.drawable.frightened);
                if(o.getMood().equals("3"))
                    holder.ImageViewMood.setBackgroundResource(R.drawable.smile);
                if(o.getMood().equals("4"))
                    holder.ImageViewMood.setBackgroundResource(R.drawable.tease);
                else
                    holder.ImageViewMood.setBackgroundResource(R.drawable.smile);


            }
        }
        return v;
    }

    public static class MessageHolder {
        public TextView TextViewName;
        public TextView TextViewMessage;
        public TextView TextViewTime;
        public ImageView ImageViewMood;
        public ImageView ImageViewHead;

    }

    private class pageStart extends AsyncTask<Void, Integer, String> {
        Bitmap bitmap;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }
        @Override
        protected String doInBackground(Void... arg0) {


             bitmap = ImageHandle.getHttpBitmap(Head);
//            bitmap = ImageHandle.getHttpBitmap("http://files.itproportal.com/wp-content/uploads/2015/01/gmail.jpg");









            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            holder.ImageViewHead.setImageBitmap(bitmap);




        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }



}
