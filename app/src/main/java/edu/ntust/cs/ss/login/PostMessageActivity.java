package edu.ntust.cs.ss.login;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.List;

import edu.ntust.cs.ss.login.lib.AESGenerator;
import edu.ntust.cs.ss.login.lib.DB;


/**
 * @author Ssu-Wei,Tang
 */
public class PostMessageActivity extends Activity {

    private DB mDbHelper = new DB(PostMessageActivity.this);

    private Button buttonCry;
    private Button buttonSmile;
    private Button buttonFrightened;
    private Button buttonTease;
    private EditText editTextMessage;
    private Button butttonPost;
    private String aesToken;
    private String loginStatus;
    private String emotion="1";
    private String content;
    private String myAesKey;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_message);
        mDbHelper.open();
        findViewById();
        setListeners();




    }


    private void findViewById() {

        buttonCry=(Button) findViewById(R.id.buttonCry);
        buttonSmile=(Button) findViewById(R.id.buttonSmile);
        buttonFrightened=(Button) findViewById(R.id.buttonFrightened);
        buttonTease =(Button) findViewById(R.id.buttonTease);
        editTextMessage=(EditText)findViewById(R.id.editTextMessage);
        butttonPost=(Button) findViewById(R.id.buttonPost);



    }
    private void setListeners() {
        buttonCry.setOnClickListener(moodClickListener);
        buttonSmile.setOnClickListener(moodClickListener);
        buttonFrightened.setOnClickListener(moodClickListener);
        buttonTease.setOnClickListener(moodClickListener);
        butttonPost.setOnClickListener(postClickListener);
    }


    private boolean deterLogin() {

        Cursor cursorLoginAccount = mDbHelper.getLoginAccount();
        while (cursorLoginAccount.moveToNext()) {
            loginStatus = cursorLoginAccount.getString(0);
        }

        cursorLoginAccount.close();

        Log.d("FB", "loginStatus" + loginStatus);

        if (loginStatus.toString().equals("T")) {
           return true;
        }
        else
            return false;




    }

    private String getSqliteToken() {

        String myToken = null;
        Cursor cursorToken = mDbHelper.getTokenData();
        while (cursorToken.moveToNext()) {
            myToken = cursorToken.getString(0);
        }
            cursorToken.close();
            return myToken;
    }

    private Button.OnClickListener moodClickListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.buttonCry:
                    emotion="1";
                    buttonCry.setBackgroundResource(R.drawable.cry2);
                    buttonFrightened.setBackgroundResource(R.drawable.frightened);
                    buttonSmile.setBackgroundResource(R.drawable.smile);
                    buttonTease.setBackgroundResource(R.drawable.tease);
                    break;

                case R.id.buttonFrightened:
                    emotion="2";
                    buttonCry.setBackgroundResource(R.drawable.cry);
                    buttonFrightened.setBackgroundResource(R.drawable.frightened2);
                    buttonSmile.setBackgroundResource(R.drawable.smile);
                    buttonTease.setBackgroundResource(R.drawable.tease);
                    break;
                case R.id.buttonSmile:
                    emotion="3";
                    buttonCry.setBackgroundResource(R.drawable.cry);
                    buttonFrightened.setBackgroundResource(R.drawable.frightened);
                    buttonSmile.setBackgroundResource(R.drawable.smile2);
                    buttonTease.setBackgroundResource(R.drawable.tease);

                    break;

                case R.id.buttonTease:
                    emotion="4";
                    buttonCry.setBackgroundResource(R.drawable.cry);
                    buttonFrightened.setBackgroundResource(R.drawable.frightened);
                    buttonSmile.setBackgroundResource(R.drawable.smile);
                    buttonTease.setBackgroundResource(R.drawable.tease2);
                    break;
            }
        }
    };

    private Button.OnClickListener postClickListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(editTextMessage.getText().toString().equals(""))
            {
                Toast.makeText(PostMessageActivity.this, "Please enter the message!", Toast.LENGTH_LONG).show();
            }
            else {
                content=editTextMessage.getText().toString();

                new PostMessageAsyncTask().execute();

            }

        }
    };


    public class PostMessageAsyncTask extends AsyncTask<Void, Integer, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();



        }

        @Override
        protected String doInBackground(Void... arg0) {


//            HttpClient httpclient = new DefaultHttpClient();
//            HttpPost httppost = new HttpPost("http://idsl.bfx.tw/articles");




//            try {

//                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
//                nameValuePairs.add(new BasicNameValuePair("article[token]", getToken()));
//                nameValuePairs.add(new BasicNameValuePair("article[emotion]", emotion));
//                nameValuePairs.add(new BasicNameValuePair("article[content]", content));


//                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8));
                requestHTTPSPage("https://idsl.bfx.tw/articles");


//                HttpResponse response = httpclient.execute(httppost);

//            } catch (ClientProtocolException e) {
//
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//
//            }


            return null;
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putString("myAesKey", myAesKey);
            intent.putExtras(bundle);
            intent.setClass(PostMessageActivity.this, MessageActivity.class);
            startActivity(intent);
           finish();


         

        }
        @Override
        protected void onCancelled() {
            super.onCancelled();


        }

    }

    private String getToken() {

        String deAesToken=null;
        Bundle secretBundle = PostMessageActivity.this.getIntent().getExtras();
        myAesKey = secretBundle.getString("myAesKey");


        if(deterLogin())
        {
            aesToken= getSqliteToken();

        }
        try {
            deAesToken= AESGenerator.DecryptToken(aesToken, myAesKey);

        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Token", "deAesToken" + deAesToken);
        Log.d("Token", "aesToken" + aesToken);



        return deAesToken;
    }


    @Override
        public void onDestroy() {
            mDbHelper.close();
            super.onDestroy();
        }


    private  String requestHTTPSPage(String mUrl) {
        InputStream ins = null;
        String result = "";
        try {
            ins = PostMessageActivity.this.getAssets().open("ca.crt");
            CertificateFactory cerFactory = CertificateFactory
                    .getInstance("X.509");
            Certificate cer = cerFactory.generateCertificate(ins);
            KeyStore keyStore = KeyStore.getInstance("PKCS12", "BC");
            keyStore.load(null, null);
            keyStore.setCertificateEntry("trust", cer);

            SSLSocketFactory socketFactory = new SSLSocketFactory(keyStore);
            Scheme sch = new Scheme("https", socketFactory, 443);




            HttpClient mHttpClient = new DefaultHttpClient();
            mHttpClient.getConnectionManager().getSchemeRegistry()
                    .register(sch);

            try {
                Log.d("CA", "executePost is in,murl:" + mUrl);
                HttpPost httppost = new HttpPost(mUrl);

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                nameValuePairs.add(new BasicNameValuePair("article[token]", getToken()));
                nameValuePairs.add(new BasicNameValuePair("article[emotion]", emotion));
                nameValuePairs.add(new BasicNameValuePair("article[content]", content));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
                HttpResponse response = mHttpClient.execute(httppost);

                if (response.getStatusLine().getStatusCode() != 200) {


                    return result;
                }

                System.out.println("eee"+response.toString());

            } catch (Exception e) {
                e.printStackTrace();
            } finally {

            }
        } catch (Exception e) {

        } finally {

        }
        return result;
    }






}
