package edu.ntust.cs.ss.login;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;

import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.security.cert.Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.security.cert.X509Certificate;


import edu.ntust.cs.ss.login.lib.AESGenerator;
import edu.ntust.cs.ss.login.lib.DB;
import edu.ntust.cs.ss.login.lib.SHA1Generator;


@SuppressWarnings("ALL")
public class LoginActivity extends Activity {
    private DB mDbHelper = new DB(LoginActivity.this);
    private CallbackManager callbackManager;
    private LoginButton fbLoginButton;
    String myAesKey;

    public static AccessToken accessToken;
    public static String token;
    private String secret;
    private String myhead;
    private String myName;
    private String deAesToken=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();



        setContentView(R.layout.activity_login);
        mDbHelper.open();
        getFbKeyHash("edu.ntust.cs.ss.login");
        findVIewById();



        Bundle logoutBundle = LoginActivity.this.getIntent().getExtras();
        String logoutStatus = logoutBundle.getString("logout");
        if(logoutStatus!=null) {
            if (logoutStatus.toString().equals("T")) {
                LoginManager mLoginManager = LoginManager.getInstance();
                mLoginManager.logOut();
            }
        }



        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {


                Log.d("FB", "loginResult" + loginResult);
                Log.d("FB", "User ID" + loginResult.getAccessToken().getUserId());
                Log.d("FB", "Token" + loginResult.getAccessToken().getToken());

                Toast.makeText(LoginActivity.this, "Login Successful!", Toast.LENGTH_LONG).show();


                accessToken = loginResult.getAccessToken();
                token = accessToken.getToken();

                Bundle secretBundle = LoginActivity.this.getIntent().getExtras();
                String fbId=loginResult.getAccessToken().getUserId();


                 secret = secretBundle.getString("Secret");
                myAesKey = secretBundle.getString("myAesKey");


                String myAesToken = null;
                try {
                    myAesToken = AESToken(token, secret);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                insertShaSecret(SHA1Generator.generateSha1ForString(secret.concat(fbId)));




                insertToken(myAesToken);
                insterAccount("T", loginResult.getAccessToken().getUserId());



                new CreateMemberAsyncTask().execute();


            }


            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Login cancelled by user!", Toast.LENGTH_LONG).show();


            }

            @Override
            public void onError(FacebookException e) {
                Toast.makeText(LoginActivity.this, "Login unsuccessful!", Toast.LENGTH_LONG).show();

            }
        });


    }

    private String AESToken(String token, String secret)throws Exception {
        String enAesToken;




        enAesToken= AESGenerator.EncryptToken(token, myAesKey);




        return enAesToken;
    }

    @Override
    public void onDestroy() {
        mDbHelper.close();
        super.onDestroy();
    }

    private void insertShaSecret(String shaSecret) {

        mDbHelper.beginTransaction();
        try {


            mDbHelper.deleteShaTokenData();
            mDbHelper.insertShaTokenData(shaSecret);
            mDbHelper.transactionSuccessful();
        } catch (Exception e) {


        } finally {
            mDbHelper.endTransaction();
        }
    }

    private void insterAccount(String loginStatus, String userId) {

        mDbHelper.beginTransaction();
        try {

            mDbHelper.deleteLoginAccount();
            mDbHelper.insertLoginAccount(loginStatus, userId);
            mDbHelper.transactionSuccessful();
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            mDbHelper.endTransaction();
        }

    }

    private void insertToken(String token) {

        mDbHelper.beginTransaction();
        try {


            mDbHelper.deleteTokenData();
            mDbHelper.insertTokenData(token);
            mDbHelper.transactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            mDbHelper.endTransaction();
        }

    }

    private String getToken() {

        String AesToKen=null;
        Cursor cursorToken = mDbHelper.getTokenData();
        while (cursorToken.moveToNext()) {
            AesToKen = cursorToken.getString(0);
        }



        cursorToken.close();

        return AesToKen;


    }


    private void findVIewById() {

        fbLoginButton = (LoginButton) findViewById(R.id.fb_login_button);
        fbLoginButton.setReadPermissions(Arrays.asList("public_profile"));
    }

    protected void jumpToMessage() {
        Intent intent = new Intent();
        intent.setClass(LoginActivity.this, MessageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("myAesKey", myAesKey);

        intent.putExtras(bundle);
        startActivity(intent);
        finish();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);


    }

    public void getFbKeyHash(String packageName) {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("YourKeyHash :", Base64.encodeToString(md.digest(), Base64.DEFAULT));

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }


    public class CreateMemberAsyncTask extends AsyncTask<Void, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        @Override
        protected String doInBackground(Void... arg0) {


            String myAesToken=getToken();






            try {
                deAesToken= AESGenerator.DecryptToken(myAesToken, myAesKey);

            } catch (Exception e) {
                e.printStackTrace();
            }

//            HttpClient httpclient = new DefaultHttpClient();
//            HttpPost httppost = new HttpPost("https://idsl.bfx.tw/users");

//            cert();



//                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
//                nameValuePairs.add(new BasicNameValuePair("user[token]", deAesToken));
//                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
//                HttpResponse response = httpclient.execute(httppost);
                requestHTTPSPage("https://idsl.bfx.tw/users");














            return null;
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            jumpToMessage();

        }
        @Override
        protected void onCancelled() {
            super.onCancelled();


        }

    }

    private  String requestHTTPSPage(String mUrl) {
        InputStream ins = null;
        String result = "";
        try {
            ins = LoginActivity.this.getAssets().open("ca.crt");
            CertificateFactory cerFactory = CertificateFactory
                    .getInstance("X.509");
            Certificate cer = cerFactory.generateCertificate(ins);
            KeyStore keyStore = KeyStore.getInstance("PKCS12", "BC");
            keyStore.load(null, null);
            keyStore.setCertificateEntry("trust", cer);

            SSLSocketFactory socketFactory = new SSLSocketFactory(keyStore);
            Scheme sch = new Scheme("https", socketFactory, 443);




            HttpClient mHttpClient = new DefaultHttpClient();
            mHttpClient.getConnectionManager().getSchemeRegistry()
                    .register(sch);

            try {
                Log.d("CA", "executePost is in,murl:" + mUrl);
                HttpPost httppost = new HttpPost(mUrl);

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("user[token]", deAesToken));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
                HttpResponse response = mHttpClient.execute(httppost);

                if (response.getStatusLine().getStatusCode() != 200) {


                    return result;
                }

                System.out.println("eee"+response.toString());

            } catch (Exception e) {
                e.printStackTrace();
            } finally {

            }
        } catch (Exception e) {

        } finally {

        }
        return result;
    }





}